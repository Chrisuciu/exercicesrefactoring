package ca.qc.claurendeau.exercice_2;

// Déplacement de champ

public class Account {
  // ...
  private AccountType type;

  public double interestForAmount_days(double amount, int days) {
    return type.getInterestRate() * amount * days / 365.0;
  }
}
