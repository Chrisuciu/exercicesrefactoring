package ca.qc.claurendeau.exercice_4;

import ca.qc.claurendeau.exercice_3.Person;

class Department {
	private String chargeCode;
	private Person manager;

	public Department(Person manager) {
		this.manager = manager;
	}
	public Person getManager() {
		return manager;
	}
}
