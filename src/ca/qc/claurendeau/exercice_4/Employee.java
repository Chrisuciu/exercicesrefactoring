package ca.qc.claurendeau.exercice_4;

import ca.qc.claurendeau.exercice_3.Person;

// Suppression du "middle-man"

public class Employee {
	private Department department;

	public Department getDepartment() {
		return department;
	}
	public void setDepartment(Department arg) {
		department = arg;
	}
	
	public Person getManager() {
		return department.getManager();
	}
}

// Quelque part dans le code client
// manager = john.getDepartment().getManager();

