package ca.qc.claurendeau.exercice_1;

public class AccountType {
	
	public boolean isPremium() {
		return false;
	}
	
	public double overdraftCharge(int daysOverdrawn) {
		return daysOverdrawn * 1.75;
	}
	
}
