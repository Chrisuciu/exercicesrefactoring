package ca.qc.claurendeau.exercice_1;

public class AccountTypePremium extends AccountType {
	public boolean isPremium() {
		return true;
	}
	@Override
	public double overdraftCharge(int daysOverdrawn) {
		double result = 10;
		if (daysOverdrawn > 7) {
			result += (daysOverdrawn - 7) * 0.85;
		}
		return result;
	}
}
