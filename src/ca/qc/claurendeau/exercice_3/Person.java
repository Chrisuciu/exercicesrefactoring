package ca.qc.claurendeau.exercice_3;

// Extraction de classe

public class Person {
	private String name;
	private PhoneNumber data = new PhoneNumber();

	public PhoneNumber getData() {
		return data;
	}

	public void setData(PhoneNumber data) {
		this.data = data;
	}

	public String getName() {
		return name;
	}
	
}
