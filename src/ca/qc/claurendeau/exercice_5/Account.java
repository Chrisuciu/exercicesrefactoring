package ca.qc.claurendeau.exercice_5;

// Introduction de méthode extérieure

import java.util.Date;

public class Account {
  @SuppressWarnings("deprecation")

  double schedulePayment() {
	Date paymentDate = getNextWeek(new Date());
    // Issue a payment using paymentDate.
    // ...
	return 0.0;
  }
  
  public static Date getNextWeek(Date previousDate) {
	  return new Date(previousDate.getYear(), previousDate.getMonth(), previousDate.getDate() + 7);
  }
}
